<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: 
        Repitition Control Structures and Array Manipulation
    </title>
</head>
<body>

    <h1>Repetition Control Structures</h1>
    <h2>While Loop</h2>
    <?php whileLoop(); ?>

    <h2>Do-While Loop</h2>
    <?php doWhileLoop(); ?>

    <h2>For Loop</h2>
    <?php forLoop(); ?>
    
    <h2>Continue and Break Statements</h2>
    <?php modifiedForLoop(); ?>

    <h2>Mini-Activity</h2>
    <?php whileLoopActivity(); ?>

    <h1>Array Manipulation</h1>

    <h2>Types of Arrays</h2>
    
    <h3>Simple Array</h3>
    <ul>
       <!-- foreach() only works on array
            Syntax:
                foreach($variableName as $key => $value)
        -->
        <!-- PHP codes/statesments can be breakdown using the PHP tags -->
        <?php foreach($computerBrands as $brand){?>
            <!-- PHP includes a short hand for "php echo tag" -->
            <li> <?= $brand; ?> </li>
       <?php } ?>
    </ul>

    <h3>Associative Array</h3>
    <!-- Associative arrays use named "keys" that you assign to them  -->
    <ul>
        <?php foreach($gradePeriods as $period => $grade){ ?>
            <li>
                Grade in <?= $period ?> is <?= $grade ?></li>
        <?php } ?>
    </ul>

    <h3>Two / Multi-Dimensional Array </h3>
    <ul>
<?php
            foreach($heroes as $team){
                foreach($team as $member){ ?>
                    <li><?= $member ?></li>
<?php       } 
        } ?>
    </ul>

    <h3>Multi-Dimensional Associative Array</h3>
    <ul>
        <?php 
            foreach($ironManPowers as $label => $powerGroup){
                foreach($powerGroup as $power){?>
                <li> <?= "$label: $power" ?> </li>
        <?php   }
            }
        ?>
    </ul>

    <h2>Array Functions</h2>
    
    <h3>Original Array</h3>
    <pre><?php print_r($computerBrands); ?></pre>

    <h3>Sorting in Ascending Order</h3>
    <pre><?php print_r($sortedBrands); ?></pre>

    <h3>Sorting in Descending Order</h3>
    <pre><?php print_r($reverseSortedBrands); ?></pre>

    <h3>Append</h3>

    <h4>Add one or more element at the end of an element</h4>
    <!-- Syntax:
            array_push($array, 'value');
     -->
    <?php array_push($computerBrands, 'Apple'); ?>
    <pre><?php print_r($computerBrands);?></pre>

    <h4>Add one or more element at the start of an array.</h4>
    <!-- Syntax:
        array_unshift($array, 'value');
    -->
    <?php array_unshift($computerBrands, 'Dell'); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    
    <h3>Remove</h3>
   
    <h4>Remove the element at the end of an array</h4>
    <!-- Syntax:
        array_pop($array, 'value');
    -->
    <?php array_pop($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>

    <h4>Remove element at the start of an array.</h4>
     <!-- Syntax:
        array_shift($array, 'value');
    -->
    <?php array_shift($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    
    <h3>Others</h3>
    <h4>Count the number of elements</h4>
    <pre><?php echo count($computerBrands); ?></pre>

    <h4>in_array: Used to search a specific element in the array</h4>
    <p><?php echo searchBrands('HP', $computerBrands) ?></p>

    <h4>array_reverse: return the array in reversed order.</h4>
    <pre><?php print_r($reversedGradePeriods);  ?></pre>

</body>
</html>